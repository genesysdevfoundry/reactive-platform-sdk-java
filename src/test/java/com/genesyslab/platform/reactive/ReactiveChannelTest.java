package com.genesyslab.platform.reactive;

import com.genesyslab.platform.commons.protocol.*;
import com.genesyslab.platform.reporting.protocol.statserver.requests.RequestOpenStatistic;
import com.genesyslab.platform.voice.protocol.TServerProtocol;
import com.genesyslab.platform.voice.protocol.tserver.AddressType;
import com.genesyslab.platform.voice.protocol.tserver.ControlMode;
import com.genesyslab.platform.voice.protocol.tserver.RegisterMode;
import com.genesyslab.platform.voice.protocol.tserver.events.EventRegistered;
import com.genesyslab.platform.voice.protocol.tserver.requests.dn.RequestRegisterAddress;
import io.reactivex.Completable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import static com.genesyslab.platform.reactive.TestUtil.createTServerProtocol;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertTrue;

public class ReactiveChannelTest {
    private static final Logger logger = LogManager.getLogger();

    @Test
    public void open_channel() throws InterruptedException, ProtocolException {
        TServerProtocol tserver = createTServerProtocol();
        ReactiveChannel rchannel = new ReactiveChannel(tserver);
        Completable opened = rchannel.getEvents()
                .takeUntil(ev -> ev instanceof ChannelOpenedEvent)
                .ignoreElements()
                .cache();
        opened.subscribe();
        Thread.sleep(5000);
        tserver.beginOpen();
        assertTrue(opened.blockingAwait(5, SECONDS));
        logger.debug("opened");

        tserver.close();
    }

    @Test
    public void open_channel2_success() throws InterruptedException, ProtocolException {
        TServerProtocol tserver = createTServerProtocol(true);
        try {
            Object opened = open_channel2(tserver);
            assertTrue(opened instanceof ChannelOpenedEvent);
        } finally {
            tserver.close();
        }
    }

    @Test
    public void open_channel2_fail() throws InterruptedException, ProtocolException {
        TServerProtocol tserver = createTServerProtocol(false);
        try {
            Object opened = open_channel2(tserver);
            assertTrue(opened instanceof ChannelClosedEvent);
        } finally {
            tserver.close();
        }
    }

    private Object open_channel2(TServerProtocol tserver) {
        ReactiveChannel rchannel = new ReactiveChannel(tserver);
        return rchannel
                .doForEvents(observer -> {
                    tserver.beginOpen();
                    Thread.sleep(5000);
                })
                .takeUntil(ev -> ev instanceof ChannelOpenedEvent || ev instanceof ChannelClosedEvent)
                .lastOrError()
                .timeout(10, SECONDS)
                .blockingGet();
    }

    @Test
    public void open_channel3_success() throws Throwable {
        open_channel3(createTServerProtocol());
    }

    @Test(expected = ProtocolIOException.class)
    public void open_channel3_fail() throws Throwable {
        open_channel3(createTServerProtocol(false));
    }

    private void open_channel3(TServerProtocol tserver) throws Throwable {
        try {
            ReactiveChannel rchannel = new ReactiveChannel(tserver);
            Object opened = rchannel
                    .doForEvents(observer -> tserver.open())
                    .takeUntil(ev -> ev instanceof ChannelOpenedEvent)
                    .lastOrError()
                    .timeout(10, SECONDS)
                    .blockingGet();
            assertTrue(opened instanceof ChannelOpenedEvent);
        } catch (RuntimeException e) {
            throw e.getCause();
        } finally {
            tserver.close();
        }
    }

    @Test
    public void subscribe_twice() throws InterruptedException, ProtocolException {
        TServerProtocol tserver = createTServerProtocol();
        ReactiveChannel rchannel = new ReactiveChannel(tserver);

        Completable opened1 = rchannel.getEvents()
                .takeUntil(ev -> ev instanceof ChannelOpenedEvent)
                .ignoreElements()
                .cache();
        opened1.subscribe();

        Completable opened2 = rchannel.getEvents()
                .takeUntil(ev -> ev instanceof ChannelOpenedEvent)
                .ignoreElements()
                .cache();
        opened2.subscribe();

        tserver.beginOpen();

        Thread.sleep(5000);

        assertTrue(opened1.blockingAwait(5, SECONDS));
        logger.debug("unblocked 1");
        assertTrue(opened2.blockingAwait(5, SECONDS));
        logger.debug("unblocked 2");
    }

    @Test
    public void request_response_success() throws InterruptedException, ProtocolException {
        request_response(createRequestRegisterAddress("8001"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void request_response_fail() throws Throwable {
        request_response(RequestOpenStatistic.create());
    }

    private void request_response(Message request) throws ProtocolException, InterruptedException {
        TServerProtocol tserver = createTServerProtocol();
        ReactiveChannel rchannel = new ReactiveChannel(tserver);
        tserver.open();

        Message response = rchannel.request(request)
                .timeout(3, SECONDS, rchannel.getEventsScheduler())
                .firstOrError()
                .blockingGet();

        assertTrue(response instanceof EventRegistered);
    }

    private Message createRequestRegisterAddress(String dn) {
        RequestRegisterAddress req = RequestRegisterAddress.create();
        req.setThisDN(dn);
        req.setRegisterMode(RegisterMode.ModeShare);
        req.setControlMode(ControlMode.RegisterDefault);
        req.setAddressType(AddressType.DN);
        return req;
    }
}
