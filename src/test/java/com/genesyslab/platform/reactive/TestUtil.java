package com.genesyslab.platform.reactive;

import com.genesyslab.platform.commons.log.Log4J2LoggerFactoryImpl;
import com.genesyslab.platform.commons.protocol.Endpoint;
import com.genesyslab.platform.configuration.protocol.ConfServerProtocol;
import com.genesyslab.platform.voice.protocol.TServerProtocol;
import io.reactivex.plugins.RxJavaPlugins;

public class TestUtil {
    static {
        com.genesyslab.platform.commons.log.Log.setLoggerFactory(new Log4J2LoggerFactoryImpl());
    }

    public static TServerProtocol createTServerProtocol(boolean succeed) {
        TServerProtocol tserver = new TServerProtocol();
        tserver.setEndpoint(new Endpoint("demosrv", !succeed ? 7044 : 7004));
        return tserver;
    }

    public static TServerProtocol createTServerProtocol() {
        return createTServerProtocol(true);
    }

    public static ConfServerProtocol createConfServerProtocol() {
        ConfServerProtocol confServer = new ConfServerProtocol();
        confServer.setEndpoint(new Endpoint("demosrv", 2020));
        confServer.setClientName("Test");
        confServer.setUserName("demo");
        confServer.setUserPassword("");
        return confServer;
    }

    public static void debugObservableSubscribe() {
        RxJavaPlugins.setOnObservableSubscribe((observable, observer) -> {
            System.err.printf("OnSubscribe observer %s in observable %s\n", observer, observable);
            return observer;
        });
    }
}
