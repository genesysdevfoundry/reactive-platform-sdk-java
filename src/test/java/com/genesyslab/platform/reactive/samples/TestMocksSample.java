package com.genesyslab.platform.reactive.samples;

import com.genesyslab.platform.configuration.protocol.ConfServerProtocol;
import com.genesyslab.platform.configuration.protocol.confserver.events.EventError;
import com.genesyslab.platform.configuration.protocol.confserver.events.EventObjectsRead;
import com.genesyslab.platform.configuration.protocol.confserver.events.EventObjectsSent;
import com.genesyslab.platform.configuration.protocol.confserver.requests.objects.RequestReadObjects;
import com.genesyslab.platform.configuration.protocol.types.CfgObjectType;
import com.genesyslab.platform.reactive.ReactiveChannel;
import com.genesyslab.platform.reactive.TestUtil;
import com.genesyslab.platform.reactive.test.IPsdkChannel;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertTrue;

/** Shows how to mock an {@link IPsdkChannel} in order to perform automatic tests of a channel. */
public class TestMocksSample {
    @Test
    public void read_real() throws Exception {
        ConfServerProtocol confServer = TestUtil.createConfServerProtocol();
        ReactiveChannel channel = new ReactiveChannel(confServer);
        try {
            confServer.open();
            List<EventObjectsRead> objects = requestReadObjects(channel);
            assertTrue(!objects.isEmpty());
        } finally {
            confServer.close();
        }
    }

    @Test
    public void read_mock_success() {
        ReactiveChannel rchannel = new ReactiveChannel(new TrivialPsdkChannel() {
            @Override
            public Observable<Object> createEventsObservable() {
                EventObjectsRead ev1 = EventObjectsRead.create();
                ev1.setReferenceId(1);

                EventObjectsRead ev2 = EventObjectsRead.create();
                ev2.setReferenceId(1);

                EventObjectsSent ev3 = EventObjectsSent.create();
                ev3.setReferenceId(1);

                return Observable.just(ev1, ev2, ev3);
            }
        });

        List<EventObjectsRead> objects = requestReadObjects(rchannel);
        assertTrue(!objects.isEmpty());
    }

    @Test(expected = UnexpectedEventException.class)
    public void read_mock_fail_on_ErrorEvent() throws Throwable {
        ReactiveChannel rchannel = new ReactiveChannel(new TrivialPsdkChannel() {
            @Override
            public Observable<Object> createEventsObservable() {
                EventObjectsRead ev1 = EventObjectsRead.create();
                ev1.setReferenceId(1);

                EventError ev2 = EventError.create();
                ev2.setErrorCode(1);
                ev2.setDescription("[ERROR DESCRIPTION]");
                ev2.setReferenceId(1);

                return Observable.just(ev1, ev2);
            }
        });

        requestReadObjects(rchannel);
    }

    @Test(expected = TimeoutException.class)
    public void read_mock_fail_on_timeout() throws Throwable {
        ReactiveChannel rchannel = new ReactiveChannel(new TrivialPsdkChannel() {
            @Override
            public Observable<Object> createEventsObservable() {
                return PublishSubject.create();
            }
        });

        try {
            requestReadObjects(rchannel);
        } catch (RuntimeException e) {
            throw e.getCause();
        }
    }

    private List<EventObjectsRead> requestReadObjects(ReactiveChannel rchannel) {
        RequestReadObjects request = RequestReadObjects.create();
        request.setObjectType(CfgObjectType.CFGDN.ordinal());

        return rchannel.request(request)
                .flatMap(ev -> ev instanceof EventError ?
                        Observable.error(new UnexpectedEventException(ev)) :
                        Observable.just(ev))
                .takeWhile(ev -> !(ev instanceof EventObjectsSent))
                .cast(EventObjectsRead.class)
                .toList()
                .timeout(10, TimeUnit.SECONDS)
                .blockingGet();
    }
}
