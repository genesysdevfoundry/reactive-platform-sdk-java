package com.genesyslab.platform.reactive.samples;

import com.genesyslab.platform.commons.protocol.ChannelOpenedEvent;
import com.genesyslab.platform.commons.protocol.Endpoint;
import com.genesyslab.platform.commons.protocol.Message;
import com.genesyslab.platform.reactive.ReactiveChannel;
import com.genesyslab.platform.reporting.protocol.StatServerProtocol;
import com.genesyslab.platform.reporting.protocol.statserver.*;
import com.genesyslab.platform.reporting.protocol.statserver.requests.RequestOpenStatistic;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.EventObject;

public class StatSample {
    private static Logger logger = LogManager.getLogger();

    /** This example builds a single observable stream for a Stat Server statistic that persists across channel disconnections. */
    @Test
    public void stat_sample() throws Exception {
        StatServerProtocol statServer = new StatServerProtocol();
        statServer.setEndpoint(new Endpoint("demosrv", 7006));

        // Wrap the Stat Server channel with a reactive interface
        ReactiveChannel rchannel = new ReactiveChannel(statServer);

        // Collects stat notification "segments". A "segment" understood as the period from where
        // the channel is opened, until the channel is closed.
        Subject<Observable<Message>> statInfoSegments = PublishSubject.create();

        // Just for logging purposes. Logs channel opened and closed events.
        rchannel.getEvents()
                .filter(e -> e instanceof EventObject)
                .subscribe(e -> logger.debug("EVENT " + e.getClass().getName()));

        // Reacts to every time the channel is opened, by subscribing to stat notifications,
        // and adding responses to our "segments" collection.
        rchannel.getEvents()
                .filter(e -> e instanceof ChannelOpenedEvent)
                .doOnNext(e -> logger.debug("Opening stat..."))
                .subscribe(e ->
                        statInfoSegments.onNext(
                                rchannel.request(createOpenStatRequest())));

        // Merges all stat notification segments, thus creating a single stream for all notifications
        // across channel reconnections.
        Observable<Object> statInfos = Observable.merge(statInfoSegments);
        statInfos.subscribe(
                e -> logger.debug("STAT EVENT " + e.getClass().getName()),
                e -> logger.error("STAT ERROR ", e));

        // A small scenario with two open-close "segments", each taking 5 seconds.
        statServer.open();
        Thread.sleep(5000);
        statServer.close();
        statServer.open();
        Thread.sleep(5000);
        statServer.close();
    }

    private static final String TENANT_NAME = "Environment";
    private static final String TENANT_PASSWORD = "";
    private static final String AGENT_ID = "KSippola";

    private static RequestOpenStatistic createOpenStatRequest() {
        RequestOpenStatistic request = RequestOpenStatistic.create();
        StatisticObject statObject = StatisticObject.create();
        statObject.setObjectId(AGENT_ID);
        statObject.setObjectType(StatisticObjectType.Agent);
        statObject.setTenantName(TENANT_NAME);
        statObject.setTenantPassword(TENANT_PASSWORD);
        request.setStatisticObject(statObject);

        Notification notification = Notification.create();
        notification.setMode(NotificationMode.Periodical);
        notification.setFrequency(1);
        request.setNotification(notification);

        StatisticMetric metric = StatisticMetric.create("CurrentAgentState");
        request.setStatisticMetric(metric);

        return request;
    }
}
