package com.genesyslab.platform.reactive.samples;

import com.genesyslab.platform.commons.protocol.ProtocolException;
import com.genesyslab.platform.configuration.protocol.ConfServerProtocol;
import com.genesyslab.platform.configuration.protocol.confserver.events.EventObjectsRead;
import com.genesyslab.platform.configuration.protocol.confserver.events.EventObjectsSent;
import com.genesyslab.platform.configuration.protocol.confserver.requests.objects.RequestReadObjects;
import com.genesyslab.platform.configuration.protocol.types.CfgObjectType;
import com.genesyslab.platform.reactive.ReactiveChannel;
import com.genesyslab.platform.reactive.TestUtil;
import io.reactivex.Observable;
import org.junit.Test;

public class ConfigReadSample {

    /** This example prints out the numbers of all DNs in the Genesys Configuration. */
    @Test
    public void readObjects_sample() throws InterruptedException, ProtocolException {
        ConfServerProtocol confServer = TestUtil.createConfServerProtocol();

        // Wrap the Config Server channel with a reactive interface
        ReactiveChannel rchannel = new ReactiveChannel(confServer);
        confServer.open();

        // Create request for obtaining all DNs
        RequestReadObjects request = RequestReadObjects.create();
        request.setObjectType(CfgObjectType.CFGDN.ordinal());

        // Reads all EventObjectsRead responses until EventObjectsSent is received, extracts
        // configuration objects received, gets the "number" attribute from them, and prints them out.
        rchannel.request(request)
                .takeWhile(ev -> !(ev instanceof EventObjectsSent))
                .cast(EventObjectsRead.class)
                .flatMap(ev -> Observable.fromIterable(ev.getObjects()))
                .map(cfgObj -> (String) cfgObj.getPropertyValue("number"))
                .blockingIterable()
                .forEach(System.out::println);

        confServer.close();
    }
}