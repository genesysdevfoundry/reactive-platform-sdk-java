package com.genesyslab.platform.reactive.samples.recordedscenario;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.genesyslab.platform.commons.protocol.ChannelClosedEvent;
import com.genesyslab.platform.commons.protocol.ChannelOpenedEvent;
import com.genesyslab.platform.commons.protocol.Message;
import com.genesyslab.platform.commons.protocol.ProtocolException;
import com.genesyslab.platform.json.jackson2.confserver.ConfServerModule;
import com.genesyslab.platform.json.jackson2.tserver.TServerModule;
import com.genesyslab.platform.reactive.test.IPsdkChannel;
import com.genesyslab.platform.reactive.test.PsdkChannelDecorator;
import io.reactivex.Observable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/** Decorates a {@link IPsdkChannel} for recording incoming and outgoing messages to a file. */
public class RecordingPsdkChannel extends PsdkChannelDecorator {

    protected static ObjectMapper jsonMapper = new ObjectMapper();

    static {
        jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
        jsonMapper.registerModule(new TServerModule(true));
        jsonMapper.registerModule(new ConfServerModule(true));
    }

    protected final List<Map<String, Object>> recording =
            Collections.synchronizedList(new LinkedList<Map<String, Object>>());
    protected final String scenarioName;

    public RecordingPsdkChannel(IPsdkChannel decorated, String scenarioName) {
        super(decorated);
        this.scenarioName = scenarioName;
    }

    @Override
    public void send(Message request) throws ProtocolException {
        addScenarioItem("MESSAGE_SENT", request);
        super.send(request);
    }

    @Override
    public Observable<Object> createEventsObservable() {
        Observable<Object> result = super.createEventsObservable();
        result.subscribe(
                ev -> {
                    if (ev instanceof Message) addScenarioItem("MESSAGE_RECEIVED", ev);
                    else if (ev instanceof Throwable) addScenarioItem("EXCEPTION", ev);
                    else if (ev instanceof ChannelOpenedEvent) addScenarioItem("ChannelOpenedEvent", null);
                    else if (ev instanceof ChannelClosedEvent) addScenarioItem("ChannelClosedEvent", null);
                    else throw new RuntimeException("Unsupported element for recorded scenario: " + ev.getClass());
                },
                ex -> {});
        return result;
    }

    private void addScenarioItem(String type, Object message) {
        LinkedHashMap<String, Object> item = new LinkedHashMap<>();
        item.put("type", type);
        if (message != null)
            item.put("message", message);
        recording.add(item);
    }

    public void writeFile() throws IOException {
        Path dir = Files.createDirectories(Paths.get("recorded_scenarios"));

        Path file = dir.resolve(String.format(
                "%s.%s.scenario.json",
                scenarioName,
                DateTimeFormatter.ISO_DATE_TIME.format(LocalDateTime.now())
                        .replace(":", "-")));

        jsonMapper.writeValue(file.toFile(), recording);
    }
}
