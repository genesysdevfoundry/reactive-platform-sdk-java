package com.genesyslab.platform.reactive.samples;

import com.genesyslab.platform.commons.protocol.ChannelClosedOnSendException;
import com.genesyslab.platform.commons.protocol.Message;
import com.genesyslab.platform.commons.protocol.ProtocolException;
import com.genesyslab.platform.reactive.test.IPsdkChannel;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.Executors;

public abstract class TrivialPsdkChannel implements IPsdkChannel{
    @Override
    public Object ensureReference(Message msg) { return 1; }

    @Override
    public Object getReference(Message msg) { return 1; }

    @Override
    public void send(Message request) throws ChannelClosedOnSendException, ProtocolException {}

    @Override
    public Observable<Object> createEventsObservable() { return Observable.empty(); }

    @Override
    public Scheduler createEventScheduler() { return Schedulers.from(Executors.newSingleThreadExecutor()); }
}
