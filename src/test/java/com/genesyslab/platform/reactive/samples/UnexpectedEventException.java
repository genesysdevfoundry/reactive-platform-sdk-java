package com.genesyslab.platform.reactive.samples;

import com.genesyslab.platform.commons.protocol.Message;

public class UnexpectedEventException extends RuntimeException {
    private final Message event;

    public UnexpectedEventException(Message event) {
        super(event.toString());
        this.event = event;
    }

    public Message getEvent() {
        return event;
    }
}
