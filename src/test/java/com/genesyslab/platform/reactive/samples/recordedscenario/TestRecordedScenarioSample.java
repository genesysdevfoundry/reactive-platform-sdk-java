package com.genesyslab.platform.reactive.samples.recordedscenario;

import com.genesyslab.platform.configuration.protocol.ConfServerProtocol;
import com.genesyslab.platform.configuration.protocol.confserver.events.EventError;
import com.genesyslab.platform.configuration.protocol.confserver.events.EventObjectsRead;
import com.genesyslab.platform.configuration.protocol.confserver.events.EventObjectsSent;
import com.genesyslab.platform.configuration.protocol.confserver.requests.objects.RequestReadObjects;
import com.genesyslab.platform.configuration.protocol.types.CfgObjectType;
import com.genesyslab.platform.reactive.ReactiveChannel;
import com.genesyslab.platform.reactive.TestUtil;
import com.genesyslab.platform.reactive.impl.PsdkChannel;
import com.genesyslab.platform.reactive.samples.UnexpectedEventException;
import io.reactivex.Observable;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;


/**
 * A sample for recording scenarios, but limited. Scenario can be recorded, but not
 * replayed, because ReactiveChannel/IPsdkChannel
 * does not cover the complete PSDK channel functionality, in particular, it does not provide
 * open and close operations. That means that those operations cannot be mocked, and, therefore,
 * they cannot be exercised in tests. There's a need for a more complete PSDK channel interface
 * for complete mocking.
 */
public class TestRecordedScenarioSample {
    @Rule
    public TestName testName = new TestName();

    @Test
    public void config_read() throws Exception {
        ConfServerProtocol confServer = TestUtil.createConfServerProtocol();
        RecordingPsdkChannel recordingChannel = new RecordingPsdkChannel(
                new PsdkChannel(confServer),
                testName.getMethodName());
        ReactiveChannel rchannel = new ReactiveChannel(recordingChannel);
        confServer.open();
        List<EventObjectsRead> objects = requestReadObjects(rchannel);
        assertTrue(!objects.isEmpty());
        confServer.close();
        recordingChannel.writeFile();
    }

    private List<EventObjectsRead> requestReadObjects(ReactiveChannel rchannel) {
        RequestReadObjects request = RequestReadObjects.create();
        request.setObjectType(CfgObjectType.CFGDN.ordinal());

        return rchannel.request(request)
                .flatMap(ev -> ev instanceof EventError ?
                        Observable.error(new UnexpectedEventException(ev)) :
                        Observable.just(ev))
                .takeWhile(ev -> !(ev instanceof EventObjectsSent))
                .cast(EventObjectsRead.class)
                .toList()
                .timeout(10, TimeUnit.SECONDS)
                .blockingGet();
    }
}
