package com.genesyslab.platform.reactive;

import com.genesyslab.platform.commons.protocol.ChannelOpenedEvent;
import com.genesyslab.platform.commons.protocol.ProtocolIOException;
import com.genesyslab.platform.voice.protocol.TServerProtocol;
import org.junit.Test;

import java.util.EventObject;

import static com.genesyslab.platform.reactive.TestUtil.createTServerProtocol;
import static org.junit.Assert.assertTrue;

public class ReactiveCompletionAdapterTest {
    @Test
    public void open_channel_async_success() throws Throwable {
        TServerProtocol tserver = createTServerProtocol();
        open_channel_async(tserver);
        tserver.close();
    }

    @Test(expected = ProtocolIOException.class)
    public void open_channel_async_fail() throws Throwable {
        TServerProtocol tserver = createTServerProtocol(false);
        open_channel_async(tserver);
        tserver.close();
    }

    public void open_channel_async(TServerProtocol tserver) throws Throwable {
        ReactiveCompletionAdapter<EventObject> opened = new ReactiveCompletionAdapter<>();
        tserver.openAsync(20000, opened.getCompletionHandler(),null);
        try {
            EventObject ev = opened.getSingle().blockingGet();
            assertTrue(ev instanceof ChannelOpenedEvent);
        } catch (RuntimeException e) {
            throw e.getCause();
        }
    }
}