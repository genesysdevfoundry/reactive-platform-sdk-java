package hacking;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observables.ConnectableObservable;
import org.junit.Test;

public class Hacks {

    @Test
    public void mytest() {
        ConnectableObservable<Integer> publish = Observable.just(1, 2, 3).publish();
        publish.subscribe(System.out::println);
        publish.subscribe(System.err::println);
        Disposable disposable = publish.connect();
//        disposable.dispose();

        publish.connect();
    }

    @Test
    public void rx_cast_check() {
        Observable.just(1, "one")
                .cast(String.class)
                .toList()
                .subscribe(list -> list.forEach(System.err::println));
    }
}
