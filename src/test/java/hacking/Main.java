package hacking;

import com.genesyslab.platform.commons.log.SimpleLoggerFactoryImpl;
import com.genesyslab.platform.commons.protocol.*;
import com.genesyslab.platform.configuration.protocol.ConfServerProtocol;
import com.genesyslab.platform.configuration.protocol.confserver.events.EventObjectsRead;
import com.genesyslab.platform.configuration.protocol.confserver.events.EventObjectsSent;
import com.genesyslab.platform.configuration.protocol.confserver.requests.objects.RequestReadObjects;
import com.genesyslab.platform.configuration.protocol.types.CfgObjectType;
import com.genesyslab.platform.reactive.ReactiveChannel;
import com.genesyslab.platform.reporting.protocol.StatServerProtocol;
import com.genesyslab.platform.reporting.protocol.statserver.*;
import com.genesyslab.platform.reporting.protocol.statserver.requests.RequestCloseStatistic;
import com.genesyslab.platform.reporting.protocol.statserver.requests.RequestOpenStatistic;
import com.genesyslab.platform.voice.protocol.TServerProtocol;
import com.genesyslab.platform.voice.protocol.tserver.AddressType;
import com.genesyslab.platform.voice.protocol.tserver.ControlMode;
import com.genesyslab.platform.voice.protocol.tserver.RegisterMode;
import com.genesyslab.platform.voice.protocol.tserver.requests.dn.RequestRegisterAddress;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

// Old examples
public class Main {
    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) throws Exception {
        com.genesyslab.platform.commons.log.Log.setLoggerFactory(new SimpleLoggerFactoryImpl(System.out));

        runExampleTServer();
    }

    private static final String DN = "8011";

    /*
    Alternative scenarios:

    ->> Create an observable with the useful responses. Let the client modify it. Then send the request. But the send request
      must have its exceptions be introduced to the observable result, so it is not simply the client calling channel.send(),
      we need to offer a method to do that.

    - To do a replay observable. Send the request. Let the client modify it. But then event processing code would be called
      from 2 different threads, the calling one, and the PSDK invoker thread. Replay also does buffering which adds an
      efficiency penalty.
     */

    private static void runExampleTServer() throws Exception {
        TServerProtocol tserver = new TServerProtocol();
        tserver.setEndpoint(new Endpoint("demosrv", 7004));
        //tserver.setClientName("ReactiveTest");

        runExampleTServer(tserver);
    }

    private static void runExampleTServer(ClientChannel tserver) throws Exception {
        ReactiveChannel channel = new ReactiveChannel(tserver);
        tserver.open();

        channel.getEvents().subscribe(
                e -> logger.debug("EVENT %s", e),
                e -> logger.debug("EVENT Error", e),
                () -> logger.debug("EVENT OnComplete"),
                d -> logger.debug("EVENT OnSubscribe %s", d));

        try {
            RequestRegisterAddress reqRegister = RequestRegisterAddress.create();
            reqRegister.setThisDN(DN);
            reqRegister.setRegisterMode(RegisterMode.ModeShare);
            reqRegister.setControlMode(ControlMode.RegisterDefault);
            reqRegister.setAddressType(AddressType.DN);

            Observable<Message> responses = channel.request(reqRegister);
            responses.subscribe(e -> logger.debug("RESPONSE %s", e), e -> logger.debug("RESPONSE %s", e));

            Scheduler scheduler = Schedulers.from(command -> {
                //noinspection deprecation
                tserver.getInvoker().invoke(command);
            });

            Subject<Message> sendErrored = PublishSubject.create();

            Single<Message> first = responses
                    .mergeWith(sendErrored)
                    .timeout(3, TimeUnit.SECONDS, scheduler)
                    .firstOrError();

            first.subscribe(e -> logger.debug("FIRST %e", e), e -> logger.debug("FIRST %s", e));

            try {
                tserver.send(reqRegister);
            } catch (Throwable e) {
                sendErrored.onError(e);
            }

            Thread.sleep(6000);

        } finally {
            tserver.close();
        }
    }
}
