package com.genesyslab.platform.reactive;

import com.genesyslab.platform.commons.protocol.ClientChannel;
import com.genesyslab.platform.commons.protocol.Message;
import com.genesyslab.platform.reactive.impl.PsdkChannel;
import com.genesyslab.platform.reactive.test.IPsdkChannel;
import com.genesyslab.platform.reactive.util.ObservableUtil;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

/**
 * <p>Wraps a Platform SDK channel in order to expose its events in a reactive way. Additionally, provides some useful
 * methods to work with the channel reactively.
 *
 * <p>This class doesn't manage the opening and closing procedures of the Platform SDK channel. In particular, it
 * doesn't close the channel when the events observable is disposed of. The channel lifecycle must be managed
 * by the client.
 */
public class ReactiveChannel {
    private final IPsdkChannel channel;
    private final Observable<Object> channelEvents;
    private final Scheduler scheduler;

    /** Use this constructor for mocking the wrapped Platform SDK channel. */
    public ReactiveChannel(IPsdkChannel channel) {
        this.channel = channel;
        this.channelEvents = channel.createEventsObservable();
        this.scheduler = channel.createEventScheduler();
    }

    /** Use this constructor for wrapping a real Platform SDK channel. */
    public ReactiveChannel(ClientChannel channel) {
        this(new PsdkChannel(channel));
    }

    /** @return See {@link IPsdkChannel#createEventsObservable()} for the events you can expect. */
    public Observable<Object> getEvents() { return channelEvents; }

    /** See {@link IPsdkChannel#createEventScheduler()} */
    public Scheduler getEventsScheduler() { return scheduler; }

    /**
     * Returns the stream of events, executing an action just after subscribing to the channel events.
     *
     * @param onAfterSubscribe <p>Action to execute when the returned observable is subscribed to.
     *                         Observer is provided as a parameter in case the action wants to emit items as a
     *                         result of the action. If this action throws an exception, then it will be emitted
     *                         as an error through the returned observable.
     *
     * @return Hot observable that will execute an action when subscribed to. It emits all channel events, and
     * any additional items the onAfterSubscribe action emits.
     */
    public Observable<Object> doForEvents(Consumer<Observer<? super Object>> onAfterSubscribe) {
        return ObservableUtil.doAfterSubscribe(channelEvents, onAfterSubscribe);
    }

    /**
     * Same as {@link #doForEvents(Consumer)}, but this one takes an {@link Action} with no observer parameter.
     * Use this when the onAfterSubscribe action does not emit any items.
     */
    public Observable<Object> doForEvents(Action onAfterSubscribe) {
        return doForEvents(obs -> onAfterSubscribe.run());
    }

    /**
     * Sends a request through the channel, and returns response messages for that request.
     *
     * @param request Request to send. It is sent when the returned {@link Observable} is subscribed to.
     *
     * @return Hot observable with response messages for request.
     */
    public Observable<Message> request(Message request) {
        Object reqRef = channel.ensureReference(request);

        return doForEvents(() -> channel.send(request))
                .filter(ev -> ev instanceof Message
                        && reqRef.equals(channel.getReference((Message)ev)))
                .cast(Message.class);
    }
}
