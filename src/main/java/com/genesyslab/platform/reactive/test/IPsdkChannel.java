package com.genesyslab.platform.reactive.test;

import com.genesyslab.platform.commons.protocol.*;
import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * Indirection interface to a PSDK channel. This allows mocking the interface for doing tests without involving an
 * actual connection.
 */
public interface IPsdkChannel {

    /** Updates reference of message if needed. */
    Object ensureReference(Message msg);

    Object getReference(Message msg);

    void send(Message request) throws ChannelClosedOnSendException, ProtocolException;

    /**
     * @return <p>Hot observable that emits events received from the channel.
     *
     * <p>Events can be of class:
     * <ul>
     *     <li>{@link Message}: for incoming messages from the server</li>
     *     <li>{@link ChannelOpenedEvent}: when channel opens</li>
     *     <li>{@link ChannelClosedEvent}: when channel closes</li>
     *     <li>{@link ChannelErrorEvent}: when there is a channel error (client is not expected to handle these errors)</li>
     * </ul>
     */
    Observable<Object> createEventsObservable();

    /** @return A {@link Scheduler} that uses the channel invoker thread. */
    Scheduler createEventScheduler();
}
