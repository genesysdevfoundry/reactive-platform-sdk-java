package com.genesyslab.platform.reactive.test;

import com.genesyslab.platform.commons.protocol.ChannelClosedOnSendException;
import com.genesyslab.platform.commons.protocol.Message;
import com.genesyslab.platform.commons.protocol.ProtocolException;
import io.reactivex.Observable;
import io.reactivex.Scheduler;

public abstract class PsdkChannelDecorator implements IPsdkChannel {

    protected final IPsdkChannel decorated;

    protected PsdkChannelDecorator(IPsdkChannel decorated) {
        this.decorated = decorated;
    }
    
    @Override
    public Object ensureReference(Message msg) {
        return decorated.ensureReference(msg);
    }

    @Override
    public Object getReference(Message msg) {
        return decorated.getReference(msg);
    }

    @Override
    public void send(Message request) throws ChannelClosedOnSendException, ProtocolException {
        decorated.send(request);
    }

    @Override
    public Observable<Object> createEventsObservable() {
        return decorated.createEventsObservable();
    }

    @Override
    public Scheduler createEventScheduler() {
        return decorated.createEventScheduler();
    }
}
