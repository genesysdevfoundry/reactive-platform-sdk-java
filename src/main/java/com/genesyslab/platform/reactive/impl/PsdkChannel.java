package com.genesyslab.platform.reactive.impl;

import com.genesyslab.platform.commons.protocol.*;
import com.genesyslab.platform.reactive.test.IPsdkChannel;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.schedulers.Schedulers;

import java.util.EventObject;

/**
 * Implementation of the {@link IPsdkChannel} interface with a real PSDK channel.
 */
public class PsdkChannel implements IPsdkChannel {
    private final ClientChannel channel;

    public PsdkChannel(ClientChannel channel) {
        this.channel = channel;
    }

    @Override
    public Object ensureReference(Message msg) {
        ReferenceBuilder refBuilder = channel.getReferenceBuilder();
        Object auxReqRef = refBuilder.retreiveReference(msg);
        return auxReqRef == null ?
                refBuilder.updateReference(msg) :
                auxReqRef;
    }

    @Override
    public Object getReference(Message msg) {
        return channel.getReferenceBuilder().retreiveReference(msg);
    }


    @Override
    public void send(Message request) throws ChannelClosedOnSendException, ProtocolException {
        channel.send(request);
    }

    /** @return Hot observable with messages received and channel events. */
    // The possibility of opening and closing the Channel on subscribe was evaluated, but neglected, because:
    // - Calling observable.dispose() for closing is not a good idea, as the observable will be abruptly cancelled,
    //   leaving any observers without completion.
    // - Client code for open and wait for the ChannelOpened/ClosedEvent is tricky anyway, because the client
    //   has to take care of subscribing before calling open, in order not to risk to lose the event.
    // - Open can be done synchronously and asynchronously by the client. It is more understandable to leave
    //   the desired open procedure to the client, instead of offering options here for the open operation.
    @Override
    public Observable<Object> createEventsObservable() {
        ConnectableObservable<Object> result = Observable.create(emitter -> {
            channel.setMessageHandler(message -> emitter.onNext(message));

            channel.addChannelListener(new ChannelListener() {
                // A short inspection of Platform SDK code seems to indicate that:
                // - None of these callbacks are called with a null event parameter. Therefore, it is ok to emit the
                //   event through an observable.
                // - onChannelOpened is always called with a parameter of type ChannelOpenedEvent, so clients should not
                //   actually care about the more general EventObject type.
                @Override
                public void onChannelOpened(EventObject ev) {
                    emitter.onNext(ev);
                }

                @Override
                public void onChannelClosed(ChannelClosedEvent ev) {
                    emitter.onNext(ev);
                }

                @Override
                public void onChannelError(ChannelErrorEvent ev) {
                    emitter.onNext(ev);
                }
            });
        })
                .publish();

        // Subscription happens only once here, so that channel.setMessageHandler() and channel.addChannelListener()
        // are called only once, and before the channel is open.
        result.connect();

        return result;
    }

    @Override
    public Scheduler createEventScheduler() {
        return Schedulers.from(command -> {
            // noinspection deprecation
            channel.getInvoker().invoke(command);
        });
    }
}
