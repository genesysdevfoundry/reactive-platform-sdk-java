package com.genesyslab.platform.reactive.util;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

public class ObservableUtil {

    public static <T> Observable<T> doAfterSubscribe(Observable<T> source, Consumer<Observer<? super T>> onAfterSubscribe) {
        return source.compose(upstreamObservable ->
                (ObservableSource<T>) observer -> {
                    upstreamObservable.subscribe(observer);
                    try {
                        onAfterSubscribe.accept(observer);
                    } catch (Exception e) {
                        observer.onError(e);
                    }
                });
    }
}
