package com.genesyslab.platform.reactive;

import com.genesyslab.platform.commons.threading.CompletionHandler;
import io.reactivex.Single;
import io.reactivex.subjects.SingleSubject;

/**
 * Allows using a {@link Single} where Platform SDK expects a {@link CompletionHandler}.
 */
public class ReactiveCompletionAdapter<T> {

    private final SingleSubject<T> subject = SingleSubject.create();

    private final CompletionHandler<T, Object> completionHandler = new CompletionHandler<T, Object>() {
        @Override
        public void completed(T result, Object attachment) {
            subject.onSuccess(result);
        }

        @Override
        public void failed(Throwable throwable, Object attachment) {
            subject.onError(throwable);
        }
    };

    public Single<T> getSingle() {
        return subject;
    }

    public CompletionHandler<T, Object> getCompletionHandler() {
        return completionHandler;
    }
}
