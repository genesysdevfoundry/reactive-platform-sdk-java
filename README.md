# Reactive API for Platform SDK Channels

This small library provides adapters for handling Platform SDK channel events through an [RxJava](https://github.com/ReactiveX/RxJava/wiki) [`Observable`](http://reactivex.io/RxJava/2.x/javadoc/io/reactivex/Observable.html) interface.

This allows handling channel events in a declarative way through reactive stream operators and subscriptions. 

[Javadoc](https://genesysdevfoundry.bitbucket.io/com.genesys.platform.reactive/javadoc/)

## Introduction 

### Class `ReactiveChannel`

[`ReactiveChannel`](https://genesysdevfoundry.bitbucket.io/com.genesys.platform.reactive/javadoc/com/genesyslab/platform/reactive/ReactiveChannel.html) extends a Platform SDK channel with methods to use the channel in a reactive way.

This example prints out the numbers of all DNs in the Genesys Configuration.

    ConfServerProtocol confServer = ...;

    // Wrap the Config Server channel with a reactive interface
    ReactiveChannel rchannel = new ReactiveChannel(confServer);
    confServer.open();

    // Create request for obtaining all DNs
    RequestReadObjects request = RequestReadObjects.create();
    request.setObjectType(CfgObjectType.CFGDN.ordinal());

    // Reads all EventObjectsRead responses until EventObjectsSent is received, extracts
    // configuration objects received, gets the "number" attribute from them, and prints them out.
    rchannel.request(request)
            .takeWhile(ev -> !(ev instanceof EventObjectsSent))
            .cast(EventObjectsRead.class)
            .flatMap(ev -> Observable.fromIterable(ev.getObjects()))
            .map(cfgObj -> (String) cfgObj.getPropertyValue("number"))
            .blockingIterable()
            .forEach(System.out::println);

This example builds a single observable stream for a Stat Server statistic that persists across channel disconnections.
    
    StatServerProtocol statServer = ...;

    // Wrap the Stat Server channel with a reactive interface
    ReactiveChannel rchannel = new ReactiveChannel(statServer);

    // Collects stat notification "segments". A "segment" understood as the period from where
    // the channel is opened, until the channel is closed.
    Subject<Observable<Message>> statInfoSegments = PublishSubject.create();

    // Reacts to every time the channel is opened, by subscribing to stat notifications,
    // and adding responses to our "segments" collection.
    rchannel.getEvents()
            .filter(e -> e instanceof ChannelOpenedEvent)
            .doOnNext(e -> logger.debug("Opening stat..."))
            .subscribe(e ->
                    statInfoSegments.onNext(
                            rchannel.request(createOpenStatRequest())));

    // Merges all stat notification segments, thus creating a single stream for all notifications
    // across channel reconnections.
    Observable<Object> statInfos = Observable.merge(statInfoSegments);
    statInfos.subscribe(
            e -> logger.debug("STAT EVENT " + e.getClass().getName()),
            e -> logger.error("STAT ERROR ", e));

    // A small scenario with two open-close "segments", each taking 5 seconds.
    statServer.open();
    Thread.sleep(5000);
    statServer.close();
    statServer.open();
    Thread.sleep(5000);
    statServer.close();

Please find more examples in the `com.genesyslab.platform.reactive.samples` package, inside the `src/test` folder.

### Class `ReactiveCompletionAdapter`

Allows using Platform SDK async operations (with CompletionHandler callbacks) using RxJava's Single class.

Example:

    ReactiveCompletionAdapter<EventObject> opened = new ReactiveCompletionAdapter<>();
    tserver.openAsync(20000, opened.getCompletionHandler(),null);
    EventObject ev = opened.getSingle().blockingGet();
